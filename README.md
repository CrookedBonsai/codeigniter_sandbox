# Codeigniter Sandbox #



### What is this repository for? ###

A place to experiment with Codeigniter and run mini projects and experiments, all housed withing the Codeigniter Framework




### Setup ###
* update the feed_upload/index.php ENVIRONMENT parameter to suite the desired environment
* update the following two files, replacing 'ENVIRONMENT', in the path, with the environment set in the previous step. Copy the associated template files for the basic structure of these two files:
	* feed_upload/config/'ENVIRONMENT'/config.php
	* feed_upload/config/'ENVIRONMENT'/database.php




### Databases Required ###
The feed Upload project requires a database of the following format:



```
#!MySQL

CREATE TABLE feed_upload_example (
  id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  category VARCHAR(255) DEFAULT NULL,
  sub_category VARCHAR(255) DEFAULT NULL,
  part_number VARCHAR(255) DEFAULT NULL,
  description VARCHAR(255) DEFAULT NULL,
  timestamp_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 282
AVG_ROW_LENGTH = 233
CHARACTER SET utf8
COLLATE utf8_general_ci
ROW_FORMAT = DYNAMIC;
```



### Current Projects ###
* Feed Upload
	* This is a simple application to upload a dummy feed and then examine its contents.