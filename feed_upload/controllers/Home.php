<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        
        $this->load->Model('home_model');
        $this->load->library('session');
    }
    
    /**
     * Index
     * The home page for this product
     * Provides two menu options:
     * 1 - Upload Feed
     * 2 - View Feed
     */
	public function index()
	{        
        $this->load->view('layout/header', array('title' => 'Feed Upload Home'));
		$this->load->view('home');
        $this->load->view('layout/footer');
	}
}
