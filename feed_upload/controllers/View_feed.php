<?php

class View_feed extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('view_feed_model');
        $this->load->library('Ajax_pagination', 'session');
        $this->per_page = 100;
    }
    
    /**
     * Index
     * Display the uploaded feed in a table
     */
    public function index()
    {
        $data = array();
        
        $this->load->helper('url');
        $this->load->library('session');
        
        $total_rows = count($this->view_feed_model->get_feed());
        
        //pagination configuration
        $config['target']      = '#feed-list';
        $config['base_url']    = URL .'index.php/view_feed/ajax_pagination_data';
        $config['total_rows']  = $total_rows;
        $config['per_page']    = $this->per_page;
        $this->ajax_pagination->initialize($config);
        
        //get the posts data
        $data['feed'] = $this->view_feed_model->get_feed(array('limit'=>$this->per_page));
        
        // Get a list of all the categories for the category filter
        $data['categories'] = $this->view_feed_model->get_categories();
        
        //load the view
        $this->load->view('layout/header', array('title' => 'View Feed'));
        $this->load->view('view_feed', $data);
        $this->load->view('layout/footer');
    }
    
    
    /**
     * Ajax Pagination Data
     * This function is called via Ajax to capture the pagination and search/filter params
     * Then retrieve new data filtered by these parameters and redraw the table
     */
    public function ajax_pagination_data()
    {
        $page = $this->input->post('page');
        if(!$page){
            $offset = 0;
        }else{
            $offset = $page;
        }
        
        //set conditions for search
        $keywords   = $this->input->post('keywords');
        $sort_by    = $this->input->post('sort_by');
        $category   = $this->input->post('category');

        $conditions['search']['keywords']   = ( !empty($keywords)   ? $keywords : null );
        $conditions['search']['sort_by']    = ( !empty($sort_by)    ? $sort_by  : null );
        $conditions['search']['category']   = ( !empty($category)   ? $category : null );
        
        
        //total rows count
        $total_rows = count($this->view_feed_model->get_feed());
        
        //pagination configuration
        $config['target']      = '#feed-list';
        $config['base_url']    = URL .'index.php/view_feed/ajax_pagination_data';
        $config['total_rows']  = $total_rows;
        $config['per_page']    = $this->per_page;
        $config['link_func']   = 'searchFilter';
        $this->ajax_pagination->initialize($config);
        
        //set start and limit
        $conditions['start'] = $offset;
        $conditions['limit'] = $this->per_page;
        
        //get the posts data
        $data['feed'] = $this->view_feed_model->get_feed($conditions);
        
        //load the view
        $this->load->view('ajax-pagination-data', $data, false);
    }
}
