<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct();
        
        $this->load->Model('upload_model');
        $this->load->helper('Form', 'url');
        $this->load->library('form_validation');
    }
    
    /**
     * Index
     * Enables the uploading of a feed file after checking it is in the correct format
     */
	public function index()
	{   
        $this->load->helper('url');
        $this->load->library('session');
        
        $error = false;
        
        // Create the Feed Upload Form
        $view_data['form'] = $this->upload_model->create_feed_upload_form();
        
        $this->form_validation->set_rules('upload', 'Feed Upload');
        
        // Capture the form POST data and process the feed
        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $config['upload_path']      = './uploads/';
            $config['allowed_types']    = 'xls|xlsx';
            $config['max_size']         = '100';
            $config['overwrite']        = true;
            $config['file_name']        = $this->create_random_file_name();
            $this->load->library('upload', $config);

            // Save the file to the server and set the error flag if it fails
            $error = ( $this->upload->do_upload() ? $error : true );
            if ($error){
                $error_message = "Unable to upload the file. Please make sure it is in the correct format. Only xls and xlsx accepted.";
            }

            // If saving the file is successful, import it to the database
            if (!$error)
            {
                // Import the file to the database
                $extension      = strstr($_FILES['userfile']['name'], '.xls');
                $file_path      = $config['upload_path'] . $config['file_name'] . $extension;
                $import_result  = $this->upload_model->upload_feed_file($file_path);
                $error          = ( $import_result['result']        ? $error                            : true );
                $error_message  = ( $import_result['error_message'] ? $import_result['error_message']   : null );
            }
            
            if (!$error)
            {
                // Set success message as Flash Data so it is automatically cleared after being outputted
                $_SESSION['message']        = 'You have successfully uploaded the feed.';
                $_SESSION['messagestate']   = 'success';
                $this->session->mark_as_flash('message');
                $this->session->mark_as_flash('messagestate');
                
                log_message('info', 'File uploaded successsfully: ' . $config['file_name']);
                redirect('view_feed', 'refresh');
            }
            else
            {
                // Set error message as Flash Data so it is automatically cleared after being outputted
                $_SESSION['message']        = "There was a problem uploading the feed: {$error_message}";
                $_SESSION['messagestate']   = 'error';
                $this->session->mark_as_flash('message');
                $this->session->mark_as_flash('messagestate');
                
                log_message('debug', 'Error uploading file: ' . $config['file_name'] . ( $error_message ? ' - Error: ' . $error_message : null ));
            }
        }
        
        // Load The View, passing it the form data
        $this->load->view('layout/header', array('title' => 'Upload Feed'));
		$this->load->view('upload', $view_data);
        $this->load->view('layout/footer');
	}
    
    
    /**
     * Create Random File Name
     * Append the timestamp to the end of a pre-determined file name
     * to create random and unique file names for the uploads
     * Increasing annonimity and avoiding file overwrites.
     * 
     * @return string
     */
    function create_random_file_name() 
    {
        $filename = "file_upload_" . time();
        
        return $filename;
    }
    
}

