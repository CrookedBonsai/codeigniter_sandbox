    <script>
        function searchFilter(page_num) 
        {
            page_num = page_num?page_num:0;
            var keywords    = $('#keywords').val();
            var sort_by     = $('#sort_by').val();
            var category    = $('#category').val();

            $.ajax({
                type: 'POST',
                url: '<?= URL; ?>index.php/view_feed/ajax_pagination_data/'+page_num,
                data:'page='+page_num+'&keywords='+keywords+'&sort_by='+sort_by+'&category='+category,
                beforeSend: function () {
                    $('.loading').show();
                },
                success: function (html) {
                    $('#feed-list').html(html);
                    $('.loading').fadeOut("slow");
                }
            });
        }
    </script>
    
    <div class="jumbotron">
        <div class="container-jumbo">
            <h1>View Feed</h1>
        </div>
    </div>
    
    <div class="row">
        <div class="feed-search-panel">
            <div class="col-xs-3">
                <input type="text" class="search-box" id="keywords" placeholder="Search Part No. or Description" onkeyup="searchFilter()"/>
            </div>
            <div class="col-xs-2">
                <select id="sort_by" class="search-filter" onchange="searchFilter()">
                    <option value="">Sort By</option>
                    <option value="cat_asc">Category Ascending</option>
                    <option value="cat_desc">Category Descending</option>
                    <option value="sub_cat_asc">Sub Category Ascending</option>
                    <option value="sub_cat_desc">Sub Category Descending</option>
                    <option value="part_no_asc">Part No. Ascending</option>
                    <option value="part_no_desc">Part No. Descending</option>
                    <option value="desc_asc">Description Ascending</option>
                    <option value="desc_desc">Description Descending</option>
                </select>
            </div>
            <div class="col-xs-2">
                <div class="col-xs-2">
                    <select id="category" class="search-filter" onchange="searchFilter()">
                        <option value="">Filter By Category</option>
                        <?php 
                            foreach ($categories as $category)
                            {
                                ?>
                                <option value="<?= $category['category']; ?>"><?= $category['category']; ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-xs-5">
                <div class="loading" style="display: none;"><div class="content"><img src="<?= URL . 'assets/images/loading.gif'; ?>"/></div></div>
            </div>
        </div>
    </div>
        
    <div class="table">
        <div class="row">
            <div class="row">
                <div class="feed-header col-xs-2">Category</div>
                <div class="feed-header col-xs-3">Sub Category</div>
                <div class="feed-header col-xs-2">Part Number</div>
                <div class="feed-header col-xs-5">Description</div>
            </div>
            <div class="feed-list" id="feed-list">
                
                <?php 
                    if (!empty($feed)) 
                    {
                        foreach ($feed as $row)
                        {
                            ?>
                            <div class="row">
                                <div class="feed-item col-xs-2"><?= $row['category']; ?></div>
                                <div class="feed-item col-xs-3"><?= $row['sub_category']; ?></div>
                                <div class="feed-item col-xs-2"><?= $row['part_number']; ?></div>
                                <div class="feed-item col-xs-5"><?= $row['description']; ?></div>
                            </div>
                            <?php
                        }
                    }
                    else
                    {
                        ?>
                        <div class="row">
                            <div class="feed-item col-xs-12">
                                <div class="empty">No results found.</div>
                            </div>
                        </div>
                        <?php 
                    } 
                ?>
                <?= $this->ajax_pagination->create_links(); ?>
            </div>
            
        </div>
    </div>
</div>
    
