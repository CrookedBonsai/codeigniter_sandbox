   
<div class="jumbotron">
    <div class="container-jumbo">
        <h1>Upload Feed</h1>
        <p>Select the feed file you would like to upload</p>
        <br>

        <?= form_open_multipart('upload', $form['form_params']); ?>

        <div class="row">
            <div class="col-xs-12">    
                <?= form_label($form['userfile']['label']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">    
                <?= form_upload($form['userfile']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-error"><?= form_error('upload'); ?></div>
            </div>
        </div>
        <div class="row">    
            <div class="col-xs-12">    
                <?= form_submit($form['submit']); ?>
            </div>
        </div>
        <?= form_close(); ?>
        </form>

    </div>
</div>

