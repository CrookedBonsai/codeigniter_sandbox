 <?php 
    if (!empty($feed)) 
    {
        foreach ($feed as $row)
        {
            ?>
            <div class="row">
                <div class="feed-item col-xs-2"><?= $row['category']; ?></div>
                <div class="feed-item col-xs-3"><?= $row['sub_category']; ?></div>
                <div class="feed-item col-xs-2"><?= $row['part_number']; ?></div>
                <div class="feed-item col-xs-5"><?= $row['description']; ?></div>
            </div>
            <?php
        }
    }
    else
    {
        ?>
        <div class="row">
            <div class="feed-item col-xs-12">
                <div class="empty">No results found.</div>
            </div>
        </div>
        <?php 
    } 
?>
<?= $this->ajax_pagination->create_links(); ?>