<div class="jumbotron">
    <div class="container-jumbo">
        <h1>Home</h1>
        <p>What would you like to do?</p>
        <br>

        <div class="row">
            <div class="col-xs-2">    
                <a href="<?= URL; ?>index.php/upload/">
                    <span class="btn btn-default">Upload Feed</span>
                </a>
            </div>
            <div class="col-xs-2">    
                <a href="<?= URL; ?>index.php/view_feed/">
                    <span class="btn btn-default">View Feed</span>
                </a>
            </div>
            <div class="col-xs-8"></div>

        </div>
    </div>
</div>