<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class View_feed_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        
        $this->load->database();
    }
    
    
    function get_feed($params = array())
    {
        $sql = 'SELECT * FROM feed_upload_example WHERE id IS NOT NULL ';
        
        if (isset($params['search']['keywords']))
        {
            $params['search']['keywords']   = rtrim(ltrim($this->db->escape($params['search']['keywords']), "'"), "'");
        }
        
        // filter data by searched keywords
        if(!empty($params['search']['keywords'])){
            $sql .= "AND (description LIKE '%{$params['search']['keywords']}%' " . 
                    "OR part_number LIKE '%{$params['search']['keywords']}%') ";
        }
        // filter data by category
        if(!empty($params['search']['category']))
        {
            $sql .= " AND category = {$this->db->escape($params['search']['category'])} ";
        }
        //sort data by ascending or desceding order
        if(!empty($params['search']['sort_by']))
        {
            switch ($params['search']['sort_by'])
            {
                case 'cat_asc':
                    $sql .= "ORDER BY category ASC ";
                    break;
                case 'cat_desc':
                    $sql .= "ORDER BY category DESC ";
                    break;
                case 'sub_cat_asc':
                    $sql .= "ORDER BY sub_category ASC ";
                    break;
                case 'sub_cat_desc':
                    $sql .= "ORDER BY sub_category DESC ";
                    break;
                case 'part_no_asc':
                    $sql .= "ORDER BY part_number ASC ";
                    break;
                case 'part_no_desc':
                    $sql .= "ORDER BY part_number DESC ";
                    break;
                case 'desc_asc':
                    $sql .= "ORDER BY description ASC ";
                    break;
                case 'desc_desc':
                    $sql .= "ORDER BY description DESC ";
                    break;
                default:
                    $sql .= "ORDER BY id ASC ";
                    break;       
            }
        }
        else
        {
            $sql .= "ORDER BY id ASC ";
        }
        
        
        if (array_key_exists("start", $params) && array_key_exists("limit", $params))
        {
            $sql .= "LIMIT {$params['limit']} OFFSET {$params['start']} ";
        }
        elseif (!array_key_exists("start", $params) && array_key_exists("limit", $params))
        {
            $sql .= "LIMIT {$params['limit']} ";
        }
        
        $query = $this->db->query($sql);
        
        $result = ( $query->num_rows() > 0 ? $query->result_array() : false );
        
        return $result;
    }
    
    
    public function get_categories()
    {
        $query = $this->db->query('SELECT DISTINCT category FROM feed_upload_example');
        
        if ($query)
        {
            $result = $query->result_array();
        }
        else
        {
            $result = array(0 => 'Categories not found');
        }
        
        return $result;
    }

}