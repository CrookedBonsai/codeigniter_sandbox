<?php

class Upload_model extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
        
        $this->load->database();
    }
    
    public function create_feed_upload_form()
    {
        $fields = array(
            'form_params' => array(
                'id'    => 'file-upload-form',
                'class' => 'file-upload-form'
            ),
            'userfile' => array(
                'name'  => 'userfile',
                'id'    => 'upload',
                'class' => 'feed-upload-input',
                'label' => 'Upload Feed:',
                'placeholder' => 'Upload feed here'
            ),
            'submit' => array(
                'name'  => 'submit',
                'value' => 'Submit',
                'class' => 'btn btn-default'
            )
        );
        return $fields;
    }
    
 
    /**
     * Upload Feed File
     * Using the PHPExcel Library, this function reads the Excel Spreadsheet by
     * breaking it down into rows, columns and individual cell data
     * Then compiles this into an array containing the hears and the data.
     * 
     * It then checks that the data from the spreadsheet has the correct number of rows
     * and that the column headings match the expected headings to ensure that the correct feed
     * is being uploaded
     * 
     * If this is all successful, it clears the feed_upload_example table in the DB
     * and then uploads the data from the spreadsheet inside a transaction to catch any failures
     * 
     * @param string $file_path
     * @return array
     * 
     */
    public function upload_feed_file($file_path)
    {
        $this->load->library('Excel');
        
        $result         = false;
        $error_message  = null;
        
        // Read the file with the PHPExcel Library
        $objPHPExcel = PHPExcel_IOFactory::load($file_path);
        
        //get only the Cell Collection
        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
        
        //extract to a PHP readable array format
        foreach ($cell_collection as $cell) 
        {
            $column     = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
            $row        = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
            
            // Collect the spreadsheet data into an array
            // The top row is the headings
            if ($row == 1) {
                $header[$row][$column]      = str_replace("'", "", $this->db->escape(strtolower(str_replace(' ', '_', $data_value))));
            } else {
                $array_data[$row][$column]  = $this->db->escape($data_value);
            }
        }
        // Compile this data into an array for traversing
        $data['header'] = $header;
        $data['values'] = $array_data;
        
        
        // Data integrity checks
        // Abort upload immeditely if the feed is in the wrong format
        $integrity_check = $this->check_feed_integrity($data);
        if (!$integrity_check['result'])
        {
            return $integrity_check;
        }
        
        // Start transaction and begin upload
        $this->db->trans_start();

            $this->db->query('TRUNCATE feed_upload_example');

            $sql = "INSERT INTO feed_upload_example (category, sub_category, part_number, description) VALUES ";

            foreach ($data['values'] as $row => $cell_values)
            {
                $sql .= '(';
                $sql .= implode(', ', $cell_values);
                $sql .= '), ';
            }
            $sql = rtrim($sql, ', ');

            $query = $this->db->query($sql);
            
            $result         = ( $query  ? true : false );      
            $error_message  = ( $result ? null : $this->db->error()['message']);

        // Automatically commit transaction on success or rollback on failure
        $this->db->trans_complete();
        
        $response = array(
            'result'        => $result,
            'error_message' => $error_message
        );
        
        return $response;
    }
    
    
    /**
     * Check Feed Integrity
     * Ensure that the feed is in the correct format with the correct columns before uploading
     * 
     * @param array $data
     * @return array
     */
    private function check_feed_integrity($data)
    {
        // Default success result will be returned if there are no errors
        $result         = true;
        $error_message  = null;
        
        // Check column count matches expect 4 four columns.
        if (count($data['values'][2]) != 4)
        {
            $result         = false;
            $error_message  = 'Invalid feed format. Incorrect number of columns. Please ensure you have 4 columns.';
        }
        // Check that the column names are correct, to identify if the feed is in the correct layout
        elseif ($data['header'][1]['A'] != 'category' 
            || $data['header'][1]['B']  != 'sub_category'
            || $data['header'][1]['C']  != 'part_number'
            || $data['header'][1]['D']  != 'description')
        {
            $result        = false;
            $error_message = 'Invalid feed format. Columns do not match. Please ensure you have the correct column headings in the feed.';
        }
        
        $response = array(
            'result'        => $result,
            'error_message' => $error_message
        );
        return $response;
    }
    
    
}
